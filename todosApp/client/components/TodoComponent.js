import React from 'react'
import TodoDetails from './TodoDetails'

class Todo extends React.Component{


    render(){
        //How to fetch params from URL
        var id = this.props.params.todoId;
        
        // finding index of clicked todo
        console.log(id)
        const index = this.props.myTodos.findIndex((todo) => {return todo.id == id} );
        console.log('Index '+ index)
        const currtodo = this.props.myTodos[index];
        console.log(currtodo)
        return <TodoDetails id = {currtodo.id} title = {currtodo.title}/>
    }
}

export default Todo;