import React from "react";

export default class Main extends React.Component {
    componentDidMount() {
        console.log(this.props.fetchData());
    }
    render() {
        return (
            <div>
                {React.cloneElement(this.props.children, this.props)}
            </div>
        )
    }
}