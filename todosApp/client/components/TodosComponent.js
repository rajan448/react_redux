import React from 'react'
import ReactDOM from 'react-dom'
import TodoDetails from './TodoDetails';

class Todos extends React.Component{
    render(){
        var allTodos = this.props.myTodos.map((todo, index)=>{
                return <TodoDetails id = {todo.id} title={todo.title} key={index}/>
        })
        return <div>
            <h1>All Todos</h1>
            {allTodos}
        </div>
    }
}
export default Todos
