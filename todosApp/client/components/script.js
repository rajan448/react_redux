import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import Main from './Main.Component'

import * as actionCreators from '../actions/action_creators'

function mapStatesToProps(state){
    return{
        myTodos: state.fetchTodos
    }
}

function mapDispachToProps(dispach){
    return bindActionCreators(actionCreators, dispach)
}

var app = connect(mapStatesToProps,mapDispachToProps)(Main)

export default app;