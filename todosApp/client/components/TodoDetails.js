import React from 'react';
import {Link} from 'react-router'
class TodoDetails extends React.Component{
    render(){
        return <div className = 'todoElement'>
            <Link to = {`todo/${this.props.id}`}>
            <em>Id: </em><span>{this.props.id}</span> <br/>
            </Link>
            <em>Title: </em><span>{this.props.title}</span>
        </div>
    }
}

export default TodoDetails;