import React from "react";
import ReactDOM from "react-dom";
import {Provider} from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import store from './store'
import App from './components/script'
import Todos from "./components/TodosComponent"
import TodoDetails from './components/TodoDetails'
import Todo from './components/TodoComponent'

var router = (
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component = {App}>
                <IndexRoute component={Todos}></IndexRoute>
                <Route path="todo/:todoId" component={Todo}> </Route>
            </Route>
        </Router>
    </Provider>
)

ReactDOM.render(router,document.getElementById('content'))