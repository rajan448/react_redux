import axios from 'axios';
// Create action object along with the payload data

export function fetchData()
{
    var request = axios.get("https://jsonplaceholder.typicode.com/todos")

    return(dispach) =>{
        request.then((data)=>{
            dispach({type: "FETCH_TODOS" , response: data})
        })
    }
}
