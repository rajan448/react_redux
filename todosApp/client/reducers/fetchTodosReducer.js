export default function fetchTodos(state = [], action){
    switch(action.type)
    {
        case "FETCH_TODOS":
            console.log('fetchTodos called')
            return action.response.data;
        default:
            return state;
    }
}