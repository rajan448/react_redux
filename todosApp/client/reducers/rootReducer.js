import {combineReducers} from 'redux';

import fetchTodos from './fetchTodosReducer'

var rootReducer = combineReducers({
        fetchTodos
    });

export default rootReducer;